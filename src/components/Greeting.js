import React from 'react';
import './Greeting.css';
import Temperature from '../common/images/thermometer.png';
import Heart from '../common/images/heart.png';
import Conversation from './Conversation';
import Voice from './Voice';

const Greeting = () => {
  let transcryptArray = [];
  let messageArray = [];
  return (
    <div className='mainContainer'>
      <div className='greetingContainer'>
        <div>{'Good morning, Viraj'}</div>
        <div>
          <div className='imageDisplay'>
            <img className='Thermo' src={Temperature} />
            <span>98 ℉</span>
          </div>
          <div className='imageDisplay'>
            <img className='Heart' src={Heart} />
            <span>82 BPM</span>
          </div>
        </div>
      </div>
      <div className='convoContainer'>
        <Conversation
          transcryptArray={transcryptArray}
          messageArray={messageArray}
        />
      </div>
      {/* <Voice/> */}
    </div>
  );
};

export default Greeting;