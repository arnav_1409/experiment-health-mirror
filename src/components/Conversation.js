import React, { useState, useEffect } from 'react';
import SpeechRecognition, {
  useSpeechRecognition
} from 'react-speech-recognition';
import './Conversation.css';
import An from '../common/voice/An.mp3';
import And from '../common/voice/And.mp3';
import As from '../common/voice/As.mp3';
import Can from '../common/voice/Can.mp3';
import Connected from '../common/voice/Connected.mp3';
import Do from '../common/voice/Do.mp3';
import How from '../common/voice/How.mp3';
import In from '../common/voice/In.mp3';
import It from '../common/voice/It.mp3';
import OK_1 from '../common/voice/OK_1.mp3';
import OK_2 from '../common/voice/OK_2.mp3';
import Say from '../common/voice/Say.mp3';
import Sorry from '../common/voice/Sorry.mp3';
import Thanks_1 from '../common/voice/Thanks_1.mp3';
import Thanks_2 from '../common/voice/Thanks_2.mp3';
import That from '../common/voice/That.mp3';
import What from '../common/voice/What.mp3';
import You from '../common/voice/You.mp3';
import axios from 'axios';
import ScoreChart from './Chart';
import VideoCall from './VideoCall';

const Conversation = ({ transcryptArray, messageArray }) => {
  const [load, setLoad] = useState(false);
  const [score, setScore] = useState(0);
  const [iframeVal, setIframeVal] = useState(false);
  const [count, setCount] = useState(0);
  const graph = <ScoreChart />
  const object = new FormData();
  object.append("bed", "8");
  object.append("slp", "2");

  const unidentifiedCom = 'Can you repeat that please?';
  const [negativeRes, setNegativeRes] = useState(false);
  const allCommands = ['hello', 'hey', 'hi', `can't sleep`, `couldn't sleep`, `can't breathe`,
    `couldn't breathe`, `feeling dizzy`, `1 hour`, `2 hours`, `3 hours`, `4 hours`, `5 hours`,
    `6 hours`, `7 hours`, `8 hours`, `9 hours`, `10 hours`, `11 hours`, `12 HOURS`, `13 HOURS`, `14 HOURS`, `15 HOURS`, `16 HOURS`, `17 HOURS`, `18 HOURS`, `19 HOURS`, `1`, `2`, `3`, `4`, `5`, `6`, `7`, `8`, `9`, `10`, `yes`, `yeah`, `sure`, `Minnesota Mississippi`, `the quick brown fox`];


  const audioSet = {
    0: An,
    1: And,
    2: As,
    3: Can,
    4: Connected,
    5: Do,
    6: How,
    7: In,
    8: It,
    9: OK_1,
    10: OK_2,
    11: Say,
    12: Sorry,
    13: Thanks_1,
    14: Thanks_2,
    15: That,
    16: What,
    17: You
  }

  const commands = [
    {
      command: ['Hello', 'HEY', 'Hi'],
      callback: command => {
        messageArray.push(`You look a little under the weather. How are you feeling today?`)
        playAudio("You", 1000, 3500)
        setNegativeRes(false);
        setCount(0);
      },
      isFuzzyMatch: true,
      fuzzyMatchingThreshold: 0.8,
      bestMatchOnly: true
    },
    {
      command: [`CAN'T SLEEP`, `COULDN'T SLEEP`],
      callback: command => {
        setTimeout(() => {
          messageArray.push(`What prevents you from sleeping well?`);
          playAudio("What", 1000, 3000)
        }, 1000);
        setNegativeRes(false);
        setCount(0);
      },
      isFuzzyMatch: true,
      fuzzyMatchingThreshold: 0.8,
      bestMatchOnly: true
    },
    {
      command: [`CAN'T BREATHE`, `COULDN'T BREATHE`, `FEELING DIZZY`],
      callback: command => {
        setTimeout(() => {
          messageArray.push(`How many hours did you sleep last night?`)
          playAudio("How", 1000, 3000)
        }, 1000);
        setNegativeRes(false);
        setCount(0);
      },
      isFuzzyMatch: true,
      fuzzyMatchingThreshold: 0.8,
      bestMatchOnly: true
    },
    {
      command: [`1 HOUR`, `2 HOURS`, `3 HOURS`, `4 HOURS`, `5 HOURS`, `6 HOURS`, `7 HOURS`, `8 HOURS`, `9 HOURS`, `10 HOURS`, `11 HOURS`, `12 HOURS`, `13 HOURS`, `14 HOURS`, `15 HOURS`, `16 HOURS`, `17 HOURS`, `18 HOURS`, `19 HOURS`],
      callback: command => {
        setTimeout(() => {
          messageArray.push(`And how long were you in bed?`)
          playAudio("And", 1000, 2500)
        }, 1000);
        setNegativeRes(false);
        setCount(0);
      },
      isFuzzyMatch: true,
      fuzzyMatchingThreshold: 1,
      bestMatchOnly: true
    },
    {
      command: [`YEAH`, `YES`],
      callback: command => {
        messageArray.push(`Say 'aahh' for 6 to 10 seconds. It's OK if you have to stop to catch breath but continue for 6 to 10 seconds. Once done, say 'Minnesota Mississippi'`)
        setIframeVal(true);
        setNegativeRes(false);
        setCount(0);
      },
      isFuzzyMatch: true,
      fuzzyMatchingThreshold: 0.8,
      bestMatchOnly: true
    },
    {
      command: [`MINNESOTA MISSISSIPPI`],
      callback: command => {
        messageArray.push(`Thanks. Analyzing. This may take a few seconds.`)
        setNegativeRes(false);
        setCount(0);
      },
      isFuzzyMatch: true,
      fuzzyMatchingThreshold: 0.5,
      bestMatchOnly: true
    },
    {
      command: [`THE QUICK BROWN FOX`],
      callback: command => {
        setNegativeRes(false);
        setIframeVal(false);
        setCount(0);
      },
      isFuzzyMatch: true,
      fuzzyMatchingThreshold: 0.5,
      bestMatchOnly: true
    },
    {
      command: [`1`, `2`, `3`, `4`, `5`, `6`, `7`, `8`, `9`, `10`],
      callback: command => {
        setTimeout(() => {
          messageArray.push(`Ok, just a minute`)
          playAudio("OK_2", 1000, 3000)
        }, 1000);
        setNegativeRes(false);
        setLoad(true);
        setCount(0);
      },
      isFuzzyMatch: true,
      fuzzyMatchingThreshold: 1,
      bestMatchOnly: true
    },

  ];

  const {
    transcript,
    interimTranscript,
    finalTranscript,
    resetTranscript,
    listening
  } = useSpeechRecognition({ commands });

  const listenContinuously = () => {
    SpeechRecognition.startListening({
      continuous: true,
      language: 'en-GB'
    });
  };

  const stopListening = () => {
    SpeechRecognition.startListening({
      continuous: false
    });
  };



  const playAudio = (strClass, playDelayTime, listenDelayTime) => {
    // debugger
    stopListening();
    SpeechRecognition.abortListening();
    setTimeout(() => {
      console.log(document.getElementsByClassName(strClass))
      const audioEl = document.getElementsByClassName(strClass)[0]
      console.log('audioEl', audioEl);
      audioEl.play();
      setTimeout(() => {
        listenContinuously();
      }, listenDelayTime)
    }, playDelayTime);

  }

  const getScore = async () => {
    try {
      const scoreResult = await axios.post(`http://65.0.4.204/d_score`, object)
      console.log(scoreResult.data.scr);
      setScore(scoreResult.data.scr);
      setTimeout(() => {
        transcryptArray.push('');
        messageArray.push('Health Score:' + " " + scoreResult.data.scr + '');
        transcryptArray.push('');
        messageArray.push(graph);
        setCount(0);
      }, 1000);
      setTimeout(() => {
        transcryptArray.push('');
        messageArray.push(`As compared to the past three months, your health score has dropped quite a bit today.`);
        playAudio("As", 1000, 6000);
        setCount(0);
      }, 4000);
      setTimeout(() => {
        transcryptArray.push('');
        messageArray.push(`Do you want to run a simple test?`);
        playAudio("Do", 8000, 2500);
        setCount(0);
      }, 6000);

      setLoad(false);
    } catch (err) {
      console.error('err', err);
    }
  };


  useEffect(() => {
    if (finalTranscript !== '') {
      console.log('Got final result:', finalTranscript);
      if (!allCommands.includes(finalTranscript)) {
        setNegativeRes(true);
        setCount(count + 1);
        console.log('count', count);
        if (count === 2) {
          window.location.reload();
        }
      } else {
        if (finalTranscript === "hi" || finalTranscript === "hello" || finalTranscript === "hey") {
          transcryptArray.push('');
        } else if (finalTranscript === "Minnesota Mississippi") {
          transcryptArray.push('...Minnesota Mississippi');
        } else if (finalTranscript === "the quick brown fox") {
          messageArray.length = 0;
          transcryptArray.length = 0;
        } else {
          transcryptArray.push(finalTranscript);
        }
      }
      console.log('transcryptArray:', transcryptArray);
      console.log('messageArray:', messageArray);
      stopListening();
    }
    listenContinuously();
  }, [finalTranscript]);

  useEffect(() => {
    if (load) {
      getScore();
    }
  }, [load])

  if (!SpeechRecognition.browserSupportsSpeechRecognition()) {
    console.log(
      'Your browser does not support speech recognition software! Try Chrome desktop, maybe?'
    );
  }

  return (
    <div className='terminalContainer'>
      {
        iframeVal ? <VideoCall /> : ''
      }
      <div className='container'>
        {
          <div className="reverseDisplay">
            {transcryptArray &&
              transcryptArray.map((data, index) => (
                <div key={index}>
                  <div>
                  </div>
                  <div className="reverseDisplay">
                    <p className="text-align-color">{transcryptArray[index]}</p>
                    {index === 5 ? <p className="color-blue">{messageArray[index]}</p> : <p>{messageArray[index]}</p>}
                  </div>
                </div>
              ))
            }
            {(negativeRes) ? <div>
              <p className="color-red">{unidentifiedCom}</p>
            </div> : ''}
          </div>
        }
        <audio className="An" >
          <source src={audioSet[0]}></source>
        </audio>
        <audio className="And" >
          <source src={audioSet[1]}></source>
        </audio>
        <audio className="As" >
          <source src={audioSet[2]}></source>
        </audio>
        <audio className="Can" >
          <source src={audioSet[3]}></source>
        </audio>
        <audio className="Connected" >
          <source src={audioSet[4]}></source>
        </audio>
        <audio className="Do" >
          <source src={audioSet[5]}></source>
        </audio>
        <audio className="How" >
          <source src={audioSet[6]}></source>
        </audio>
        <audio className="In" >
          <source src={audioSet[7]}></source>
        </audio>
        <audio className="It" >
          <source src={audioSet[8]}></source>
        </audio>
        <audio className="OK_1" >
          <source src={audioSet[9]}></source>
        </audio>
        <audio className="OK_2" >
          <source src={audioSet[10]}></source>
        </audio>
        <audio className="Say" >
          <source src={audioSet[11]}></source>
        </audio>
        <audio className="Sorry" >
          <source src={audioSet[12]}></source>
        </audio>
        <audio className="Thanks_1" >
          <source src={audioSet[13]}></source>
        </audio>
        <audio className="Thanks_2" >
          <source src={audioSet[14]}></source>
        </audio>
        <audio className="That" >
          <source src={audioSet[15]}></source>
        </audio>
        <audio className="What" >
          <source src={audioSet[16]}></source>
        </audio>
        <audio className="You" >
          <source src={audioSet[17]}></source>
        </audio>
      </div>
    </div>

  );
};

export default Conversation;