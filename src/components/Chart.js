import React from "react";
import Chart from "react-google-charts";
import './Chart.css';

const data = [['Months','Health score'],
          [0, 600],
          [1, 680],
          [2, 640],
          [3, 720],
          [4, 740],
          [5, 710],
          [6, 730],
          [7, 660],
          [8, 690],
          [9, 710],
          [10, 720],
          [11, 730]
];
const options = {
  smoothLine: true,
        hAxis: {
            minValue: 0,
            title: 'Past Three Months',
            titleTextStyle: {
                bold: 'true',
                color: '#63fcff'
            },
            textPosition: 'none',
            gridlines: {
                color: '#000000'
            },
            baselineColor: {
                color: '#63fcff'
            }
        },
        vAxis: {
            minValue: 0,
            title: 'Health Score',
            titleTextStyle: {
                bold: 'true',
                color: '#63fcff'
            },
            textStyle: {
                bold: 'true',
                color: '#63fcff'
            },
            gridlines: {
                color: '#000000'
            },
            baselineColor: {
                color: '#000000'
            }
        },
        backgroundColor: '#000000',
        legend: 'none',
        colors: ['#ffffff'],
        height: 250,
        chartArea: {
            height: '90%'
        },
};
const ScoreChart = () => {
    return (
      <div className="ChartContainer">
        <Chart
          chartType="LineChart"
          width="70%"
          height="220px"
          data={data}
          options={options}
        />
      </div>
    );
}

export default ScoreChart;
