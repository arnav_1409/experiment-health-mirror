import React from 'react';
import Iframe from 'react-iframe';

function VideoCall() {
  let identifier = Math.floor(Math.random() * 10000);
  const srcVal = "https://webex-html-app-sandbox.s3.ap-south-1.amazonaws.com/v0.9.9/index.html?meetingURL=webex4viraj.techmail4sam@webex.com&name=Viraj&email=" + identifier + "@gslab.com&smartmirror"
  return (
    <div>
      <Iframe url={srcVal}
        width="100%"
        height="450px" />
    </div>
  )
}

export default VideoCall
