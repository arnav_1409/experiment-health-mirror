import React, { Component } from 'react';
import MicRecorder from 'mic-recorder-to-mp3';
import axios from 'axios';



const Mp3Recorder = new MicRecorder({ bitRate: 128 });
export class Voice extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isRecording: false,
      blobURL: '',
      isBlocked: false,
      blob: null,
    }

  }

  start = () => {
    if (this.state.isBlocked) {
      console.log('Permission Denied');
    } else {

      debugger
      Mp3Recorder
        .start()
        .then(() => {
          this.setState({ isRecording: true });
        }).catch((e) => console.error(e));
    }
  };

  stop = () => {
    debugger
    Mp3Recorder
      .stop()
      .getMp3()
      .then(([buffer, blob]) => {
        debugger
        var newblob=new Blob([buffer], {type : 'audio/wav'});
        const blobURL = URL.createObjectURL(newblob)
        console.log('blob', blobURL)
        this.setState({ blob: newblob, blobURL : blobURL, isRecording: false });
        this.getRespScore();
      }).catch((e) => console.log(e));
  };


  getRespScore = async () => {
    debugger
    let blob = this.state.blob;
    let filename = new Date().toISOString();
    let fd = new FormData();
    fd.append("audio", blob, filename);
    try {
      debugger
      const scoreResult = await axios.post(`http://65.0.4.204/s_score_1`, fd)
      console.log('scoreResult', scoreResult)
    } catch (err) {
      console.error('err', err);
    }
  };


  render() {
    return (
      <div>
      </div>
    )
  }

  componentDidMount() {
    navigator.getUserMedia({ audio: true },
      () => {
        console.log('Permission Granted');
        this.setState({ isBlocked: false });
      },
      () => {
        console.log('Permission Denied');
        this.setState({ isBlocked: true })
      },
    );
    this.start();
    setTimeout(() => {
      this.stop()
    }, 3000)
  }
}

export default Voice